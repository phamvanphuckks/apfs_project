from neo import Gpio
from time import sleep
import sys, getopt, time    

neo = Gpio()
writepin = 2
    
neo.pinMode(writepin, neo.OUTPUT)
    
while True:
  neo.digitalWrite(writepin, neo.HIGH)
  sleep(1)
  print "Pin 2 high current state is: "+str(neo.digitalRead(writepin))
  neo.digitalWrite(writepin, neo.LOW)
  sleep(1)
  print "Pin 2 low current state is: "+str(neo.digitalRead(writepin))
